import React from "react";
import { TAI, XIU } from "./TaiXiuPage";

export default function XucXac({ XucXacArr, handleDatCuoc }) {
  const renderXucXacArr = () => {
    return XucXacArr.map((item, index) => {
      return (
        <img src={item.img} style={{ height: 100, margin: 10 }} key={index} />
      );
    });
  };
  return (
    <div className="row container mx-auto my-5">
      <button
        onClick={() => {
          handleDatCuoc(TAI);
        }}
        className="btn btn-danger col-2"
        style={{ fontSize: 40 }}
      >
        TÀI
      </button>
      <div className="py-5 col-8">{renderXucXacArr()}</div>
      <button
        onClick={() => {
          handleDatCuoc(XIU);
        }}
        className="btn btn-primary p-5 col-2"
        style={{ fontSize: 40 }}
      >
        XỈU
      </button>
    </div>
  );
}
