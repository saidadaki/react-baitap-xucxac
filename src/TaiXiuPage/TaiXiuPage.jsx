import React, { useState } from "react";
import "./game.css";
import bgGame from "../assets/bgGame.png";
import XucXac from "./XucXac";
import KetQua from "./KetQua";

export const TAI = "TÀI";
export const XIU = "XỈU";

export default function TaiXiuPage() {
  const [XucXacArr, setXucXacArr] = useState([
    {
      img: "./imgXucXac/1.png",
      giaTri: 1,
      // url path should go from index.html out to img file
    },
    {
      img: "./imgXucXac/1.png",
      giaTri: 1,
    },
    {
      img: "./imgXucXac/1.png",
      giaTri: 1,
    },
  ]);

  const [luaChon, setLuaChon] = useState(null);

  let [tongDiem, setTongDiem] = useState(0);

  const [soLuotChoi, setSoLuotChoi] = useState(0);

  const [ketQua, setKetQua] = useState(0);

  const handleDatCuoc = (value) => {
    setLuaChon(value);
  };

  const handleXucXac = () => {
    let result = null;
    let tongDiem = 0;
    let newXucXacArr = XucXacArr.map(() => {
      let number = Math.floor(Math.random() * 6) + 1;
      // random each dice by a value from 1-6
      tongDiem += number;
      return {
        img: `./imgXucXac/${number}.png`,
        giaTri: { number },
      };
    });
    setXucXacArr(newXucXacArr);

    setTongDiem(tongDiem);

    setSoLuotChoi(soLuotChoi + 1);

    if (
      (tongDiem <= 10 && luaChon == XIU) ||
      (tongDiem > 10 && luaChon == TAI)
    ) {
      result = "WIN";
    } else {
      result = "LOSE";
    }
    setKetQua(result);
  };
  return (
    <div
      className="game_container pt-5"
      style={{ backgroundImage: `url("${bgGame}")` }}
    >
      <h1>Tài Xỉu Page</h1>
      <XucXac handleDatCuoc={handleDatCuoc} XucXacArr={XucXacArr} />
      <KetQua
        ketQua={ketQua}
        soLuotChoi={soLuotChoi}
        tongDiem={tongDiem}
        luaChon={luaChon}
        handleXucXac={handleXucXac}
      />
    </div>
  );
}
