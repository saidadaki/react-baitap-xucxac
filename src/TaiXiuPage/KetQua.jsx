import React from "react";

export default function KetQua({
  luaChon,
  tongDiem,
  handleXucXac,
  soLuotChoi,
  ketQua,
}) {
  return (
    <div>
      <h2>{tongDiem}</h2>
      <button onClick={handleXucXac} className="btn btn-warning p-4">
        Chơi ngay
      </button>

      <h2 className="my-3">Bạn chọn: {luaChon}</h2>
      <h2 className="my-3">Kết Quả Lượt Này: {ketQua}</h2>
      <h2 className="my-3">Số lượt chơi: {soLuotChoi}</h2>
    </div>
  );
}
