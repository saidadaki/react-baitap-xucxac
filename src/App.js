import logo from './logo.svg';
import './App.css';
import TaiXiuPage from './TaiXiuPage/TaiXiuPage';

function App() {
  return (
    <div className="App">
      <TaiXiuPage />
    </div>
  );
}

export default App;
